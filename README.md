# Depreciation API

RESTFul API to calculate a consumer good depreciation. The consumer goods grups
and calculation logic are based on SIAF:

[SIAF - Sistema Integrado de Finanças Públicas](https://conteudo.tesouro.gov.br/manuais/index.php?option=com_content&view=article&id=1565:020330-depreciacao-amortizacao-e-exaustao-na-adm-dir-uniao-aut-e-fund&catid=749&Itemid=376)

[Check online API operations](https://depreciation-staging.herokuapp.com/swagger-ui.html) 

Running local:
[TBD]

- - -
API RESTFul para calcular a depreciação de um bem de consumo.Os grupos de bens de consumo e regras de cálculo
são baseadas no SIAF:
  
[SIAF - Sistema Integrado de Finanças Públicas](https://conteudo.tesouro.gov.br/manuais/index.php?option=com_content&view=article&id=1565:020330-depreciacao-amortizacao-e-exaustao-na-adm-dir-uniao-aut-e-fund&catid=749&Itemid=376)

[Teste a API online](https://depreciation-staging.herokuapp.com/swagger-ui.html)

Rodando localmente:
[TBD]
 ---
 [Click here to check online documentation](https://depreciation-staging.herokuapp.com/swagger-ui.html) 