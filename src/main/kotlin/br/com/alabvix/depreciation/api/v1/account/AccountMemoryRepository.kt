package br.com.alabvix.depreciation.api.v1.account

import org.springframework.stereotype.Repository

@Repository
class AccountMemoryRepository(private val fileReader: AccountFileReader) : AccountRepository {

    private val accountMap:MutableMap<Int, Account> = mutableMapOf()

    init {
        fileReader.readToMap(accountMap)
    }

    override fun findAll(): MutableSet<MutableMap.MutableEntry<Int, Account>> {
        return accountMap.entries;
    }

    override fun findById(id: Int): Account {

        return accountMap[id] ?: throw IllegalArgumentException("Conta não encontrada")
    }
}
