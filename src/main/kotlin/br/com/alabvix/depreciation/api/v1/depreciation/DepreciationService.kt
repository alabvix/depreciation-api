package br.com.alabvix.depreciation.api.v1.depreciation

import br.com.alabvix.depreciation.api.v1.account.AccountService
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.math.RoundingMode

@Service
class DepreciationService(private val accountService: AccountService) {

    fun calculateDepreciation2(payload: DepreciationPayload): DepreciationOutput {

        val account = accountService.findById(payload.accountId)
        var itemValue = payload.itemValue.setScale(4, RoundingMode.UP)

        // valor residual
        val itemResidualValue = itemValue.multiply(account.residualValue.toBigDecimal())
                .setScale(4, RoundingMode.UP)

        // valor depreciável
        val itemDepreciationValue = itemValue.minus(itemResidualValue)

        // taxa depreciavel = 100 - valor residual
        val depreciationFee = 1.00 - account.residualValue

        // depreciação anual = valor do item
        val yearDepreciation = itemValue.minus(itemResidualValue).divide(BigDecimal(account.lifespanYears))

        // taxa mensal
        val monthlyDepreciation = yearDepreciation.divide(BigDecimal(12))

        val totalMonths = account.lifespanYears * 12

        var month = incrementMonth(payload.startMonth)
        var year = payload.startYear

        var depreciationList = ArrayList<Depreciation>()

        var depreciationTotal = BigDecimal.ZERO

        for (i in 1..totalMonths) {

            itemValue = itemValue.minus(monthlyDepreciation)//.setScale(5, RoundingMode.HALF_EVEN)

            val depreciation = Depreciation(
                    year,
                    month,
                    itemValue.setScale(2, RoundingMode.HALF_UP)
            )

            month ++
            if (month == 13) {
                month = 1
                year ++
            }

            depreciationTotal = depreciationTotal.plus(monthlyDepreciation)

            depreciationList.add(depreciation)

            println(depreciation)
        }

        print(depreciationTotal.setScale(2,RoundingMode.UP))

        return DepreciationOutput(
                payload.itemName,
                payload.itemValue,
                itemResidualValue.setScale(2, RoundingMode.HALF_UP),
                itemDepreciationValue.setScale(2, RoundingMode.HALF_UP),
                monthlyDepreciation.toDouble(),
                depreciationList
        )

    }

    private fun incrementMonth(pmonth: Int): Int {
        var month = pmonth
        month ++
        if (month == 13) {
            month = 1
        }
        return month
    }

}