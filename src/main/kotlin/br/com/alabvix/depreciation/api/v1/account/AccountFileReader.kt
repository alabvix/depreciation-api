package br.com.alabvix.depreciation.api.v1.account

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.InputStream
import java.math.BigDecimal

@Component
class AccountFileReader {

    private val FILE = "contas-contabeis.txt"

    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    fun readToMap(map: MutableMap<Int, Account>): Unit {

        logger.info("Reading accounts file...")
        val inputStream: InputStream =
                this.javaClass.classLoader.getResourceAsStream(FILE)
        logger.info("Accounts file loaded")

        val lineList = mutableListOf<String>()

        var nomeConta: String = ""
        var vidaUtil: String = ""
        var valorResidual: String = ""

        var accountId: Int = 0

        inputStream.bufferedReader().useLines { lines -> lines.forEach { lineList.add(it)} }
        lineList.forEach {
            var lineBreak = false

            var index = it.indexOf("CONTA")
            if (index != -1) {
                nomeConta = it.substring(20)
            }

            index = it.indexOf("Vida")
            if (index != -1) {
                vidaUtil = it.substring(18)
            }

            index = it.indexOf("Valor")
            if (index != -1) {
                valorResidual = it.substring(16)
                lineBreak = true
            }

            if (lineBreak) {
                // cria objeto account
                if (vidaUtil != "-") {
                    valorResidual = valorResidual.dropLast(1)

                    accountId ++
                    map[accountId] = Account(accountId,
                            nomeConta.trimEnd(),
                            vidaUtil.toInt(),
                            valorResidual.toBigDecimal().divide(BigDecimal(100)).toDouble())

                }

            }

        }
        inputStream.close()

    }
}