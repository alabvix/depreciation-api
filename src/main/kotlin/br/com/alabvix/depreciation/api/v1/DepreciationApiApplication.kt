package br.com.alabvix.depreciation.api.v1

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DepreciationApiApplication

fun main(args: Array<String>) {
	runApplication<DepreciationApiApplication>(*args)
}
