package br.com.alabvix.depreciation.api.v1.account

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(
        tags = ["Account"],
        description = "Operações para conta contábil.")
@RestController
@RequestMapping("/account")
class AccountController(val service: AccountService) {

    @ApiOperation(value="Lista as contas contábeis disponíveis.")
    @GetMapping("/")
    fun list(): List<Account> {
        return service.findAll()
    }

    @ApiOperation(value="Recupera uma conta contábil por seu id.")
    @GetMapping("/{id}")
    fun getByIdlist(@PathVariable id: Int): Account {
        return service.findById(id)
    }

}