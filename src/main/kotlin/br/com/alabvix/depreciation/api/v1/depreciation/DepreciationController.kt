package br.com.alabvix.depreciation.api.v1.depreciation


import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(
        tags = ["Depreciation"],
        description = "Cálculo da depreciação.")
@RestController
@RequestMapping("/depreciation")
class DepreciationController(val service: DepreciationService) {

    @ApiOperation(value="Calcula a depreciação de um bem.")
    @PostMapping("/")
    fun calculate(@RequestBody payload:DepreciationPayload):ResponseEntity<DepreciationOutput> {

        return ResponseEntity(service.calculateDepreciation2(payload), HttpStatus.OK)
    }

}