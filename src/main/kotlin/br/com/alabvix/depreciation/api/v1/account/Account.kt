package br.com.alabvix.depreciation.api.v1.account

import java.math.BigDecimal

data class Account (
        val id: Int,

        val name: String,

        val lifespanYears: Int,

        val residualValue: Double)