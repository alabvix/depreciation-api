package br.com.alabvix.depreciation.api.v1.depreciation

import java.math.BigDecimal

data class DepreciationOutput(
        val itemName: String,
        val itemValue: BigDecimal,
        val residualValue: BigDecimal,
        val depreciableValue: BigDecimal,
        val monthlyDepreciation: Double,
        val depreciationList: List<Depreciation>
)
