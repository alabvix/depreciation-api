package br.com.alabvix.depreciation.api.v1.account

interface AccountRepository {

    fun findAll(): MutableSet<MutableMap.MutableEntry<Int, Account>>

    fun findById(id: Int): Account

}