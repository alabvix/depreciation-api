package br.com.alabvix.depreciation.api.v1.exception

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest

@ControllerAdvice
class ExceptionHandler {

    @ExceptionHandler(IllegalArgumentException::class)
    fun handle(request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(HttpStatus.BAD_REQUEST);
   }

}