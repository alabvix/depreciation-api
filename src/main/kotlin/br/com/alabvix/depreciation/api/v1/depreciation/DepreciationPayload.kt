package br.com.alabvix.depreciation.api.v1.depreciation

import java.math.BigDecimal

data class DepreciationPayload(
        val accountId: Int,
        val itemName: String,
        val itemValue: BigDecimal,
        val startMonth: Int,
        val startYear: Int
)
