package br.com.alabvix.depreciation.api.v1.depreciation

import java.math.BigDecimal

data class Depreciation(

        val year : Int,

        val month: Int,

        val value: BigDecimal
)
