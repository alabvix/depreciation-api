package br.com.alabvix.depreciation.api.v1.account

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class AccountService(private val repository: AccountRepository) {

    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    fun findAll(): List<Account> {
        logger.info("Listening alla accounts")

        val accountSet = repository.findAll()
        val accountList: MutableList<Account> = ArrayList()

        for (s in accountSet) {
            accountList.add(s.value)
        }

        return accountList
    }

    fun findById(id: Int): Account {
        return repository.findById(id)
    }

}