package br.com.alabvix.depreciation.api.v1.account

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class AccountFileReaderUnitTest {

    private val accountFileReader = AccountFileReader()

    @Test
    fun `Read txt file`(){

        val accountMap:MutableMap<Int, Account> = mutableMapOf<Int, Account>()

        accountFileReader.readToMap(accountMap)

        assertTrue(accountMap.isNotEmpty())


    }
}