package br.com.alabvix.depreciation.api.v1.depreciation


import br.com.alabvix.depreciation.api.v1.account.Account
import br.com.alabvix.depreciation.api.v1.account.AccountService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.math.BigDecimal

class DepreciationServiceUnitTest {

    private val accountService = Mockito.mock(AccountService::class.java)

    private val depreciationService = DepreciationService(accountService)

    @Test
    fun `Calculates depreciation for Monitor LG 29UM69G-BAWZ 29`() {

        // Given
        val depreciationInput = DepreciationPayload(
                accountId = 1,
                itemName = "Monitor LG 29UM69G-BAWZ 29",
                itemValue = BigDecimal(1519.05),
                startMonth = 1,
                startYear = 2021
        )

        val depreciationValues: DoubleArray =
                doubleArrayOf(1496.26, 1473.48, 1450.69, 1427.91, 1405.12, 1382.34, 1359.55, 1336.76,
                1313.98, 1291.19, 1268.41, 1245.62, 1222.84, 1200.05, 1177.26, 1154.48, 1131.69, 1108.91, 1086.12,
                1063.34, 1040.55, 1017.76, 994.98, 972.19, 949.41, 926.62, 903.83, 881.05, 858.26, 835.48, 812.69,
                789.91, 767.12, 744.33, 721.55, 698.76, 675.98, 653.19, 630.41, 607.62, 584.83, 562.05, 539.26, 516.48,
                493.69, 470.91, 448.12, 425.33, 402.55, 379.76, 356.98, 334.19, 311.41, 288.62, 265.83, 243.05, 220.26,
                197.48, 174.69, 151.91
        )

        Mockito.`when`(
                accountService.findById(1)
        ).thenReturn(
                Account(
                        id = depreciationInput.accountId,
                        name = "Computadores e Periféricos",
                        lifespanYears = 5,
                        residualValue = 0.10
                )
        )

        // When
        val depreciationOutput =
                depreciationService.calculateDepreciation2(depreciationInput)

        // Then
        Mockito.verify(accountService, Mockito.times(1)).findById(1)

        assertEquals(depreciationValues.size, depreciationOutput.depreciationList.size)
        assertEquals(BigDecimal("151.91"), depreciationOutput.residualValue)
        assertEquals(BigDecimal("1367.15"), depreciationOutput.depreciableValue)
        //assertEquals(BigDecimal("1.67"), depreciationOutput.monthlyDepreciation)

        for (index: Int in depreciationValues.indices){
            assertEquals(depreciationValues[index].toBigDecimal(), depreciationOutput.depreciationList[index].value)
        }

    }

    @Test
    fun `Calculates depreciation for Cadeira Giratória Preta`() {

        // Given
        val depreciationInput = DepreciationPayload(
                accountId = 2,
                itemName = "Cadeira Giratória Preta",
                itemValue = BigDecimal(1200.00),
                startMonth = 11,
                startYear = 2020
        )

        val depreciationValues: DoubleArray =
                doubleArrayOf(
                        1191.00, 1182.00, 1173.00, 1164.00, 1155.00, 1146.00, 1137.00, 1128.00, 1119.00, 1110.00, 1101.00,
                        1092.00, 1083.00, 1074.00, 1065.00, 1056.00, 1047.00, 1038.00, 1029.00, 1020.00, 1011.00, 1002.00,
                        993.00, 984.00, 975.00, 966.00, 957.00, 948.00, 939.00, 930.00, 921.00, 912.00, 903.00, 894.00,
                        885.00, 876.00, 867.00, 858.00, 849.00, 840.00, 831.00, 822.00, 813.00, 804.00, 795.00, 786.00,
                        777.00, 768.00, 759.00, 750.00, 741.00, 732.00, 723.00, 714.00, 705.00, 696.00, 687.00, 678.00,
                        669.00, 660.00, 651.00, 642.00, 633.00, 624.00, 615.00, 606.00, 597.00, 588.00, 579.00, 570.00,
                        561.00, 552.00, 543.00, 534.00, 525.00, 516.00, 507.00, 498.00, 489.00, 480.00, 471.00, 462.00,
                        453.00, 444.00, 435.00, 426.00, 417.00, 408.00, 399.00, 390.00, 381.00, 372.00, 363.00, 354.00,
                        345.00, 336.00, 327.00, 318.00, 309.00, 300.00, 291.00, 282.00, 273.00, 264.00, 255.00, 246.00,
                        237.00, 228.00, 219.00, 210.00, 201.00, 192.00, 183.00, 174.00, 165.00, 156.00, 147.00, 138.00,
                        129.00, 120.00
                )

        Mockito.`when`(
                accountService.findById(2)
        ).thenReturn(
                Account(
                        id = 2,
                        name = "Mobiliário em geral",
                        lifespanYears = 10,
                        residualValue =0.1
                )
        )

        // When
        val depreciationOutput =
                depreciationService.calculateDepreciation2(depreciationInput)

        // Then
        Mockito.verify(accountService, Mockito.times(1)).findById(2)

        assertEquals(depreciationValues.size, depreciationOutput.depreciationList.size)
        assertEquals(BigDecimal("120.00"), depreciationOutput.residualValue)
        assertEquals(BigDecimal("1080.00"), depreciationOutput.depreciableValue)
        //assertEquals(BigDecimal("0.83"), depreciationOutput.monthlyDepreciation)

        for (index: Int in depreciationValues.indices){
            assertEquals(depreciationValues[index].toBigDecimal().setScale(2), depreciationOutput.depreciationList[index].value)
        }

    }

}